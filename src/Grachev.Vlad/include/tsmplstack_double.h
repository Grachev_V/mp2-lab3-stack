// ������������ ��������� ������ - ������� ���������� �����

#ifndef _TSTACK_H_
#define _TSTACK_H_

#define MemSize 255 // ������ ������ ��� �����

class TSimpleStackD {
protected:
	double Mem[MemSize]; // ������ ��� ��
	int Top; // ������ ���������� �������� �������� � Mem - ������� �����
public:
	TSimpleStackD() { Top = -1; }
	bool IsEmpty() const { return Top == -1; } // �������� �������
	bool IsFull() const { return Top == MemSize - 1; } // �������� ������������
	void Push(const double Val) // �������� ��������
	{
		if (IsFull())
			throw("Stack is full");
		else
			Mem[++Top] = Val;
	}
	double Pop() // ������� ��������
	{
		if (IsEmpty())
			throw("Stack is empty");
		else
			return Mem[Top--];
	}
	double TopElem() // ���������� �������� ������� �����
	{

		if (IsEmpty())
			throw("Stack is empty");
		else
			return Mem[Top];
	}
};
#endif