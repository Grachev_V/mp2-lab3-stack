#ifndef EXP_H
#define EXPR_H

#include <iostream>
#include <string>
#include "tdatstack.h"
#include "tsmplstack_double.h"

using namespace std;

#define ExpSize 255

bool BrControl(const string &, bool); // �������� ����������� ����������� ������� ������
string ConvertToPostfix (const string &); // ������������� � ����������� ������
double Calculation(const string &);	// ���������� �������� ���������, ����������� � ���������� ����

#endif